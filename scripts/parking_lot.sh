#!/bin/sh

cd ..
printf "\nBuilding jar file with maven...\n"
mvn clean compile test assembly:single
printf "\nCreating alias for future execution...\n"
alias parking_lot='java -jar target/multi-store-parking-2.0.0.jar $1'
echo "\nStarting execution... $1\n"
parking_lot
