# Multi Store Parking - Interview Challenge

version 2.0.0 - 21/02/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/multi-store-parking.svg)](http://bitbucket.org/ivan-sanabria/multi-store-parking/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/multi-store-parking.svg)](http://bitbucket.org/ivan-sanabria/multi-store-parking/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_multi-store-parking&metric=alert_status)](https://sonarcloud.io/project/overview?id=ivan-sanabria_multi-store-parking)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_multi-store-parking&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=ivan-sanabria_multi-store-parking)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_multi-store-parking&metric=coverage)](https://sonarcloud.io/component_measures?id=ivan-sanabria_multi-store-parking&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_multi-store-parking&metric=ncloc)](https://sonarcloud.io/code?id=ivan-sanabria_multi-store-parking)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_multi-store-parking&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=ivan-sanabria_multi-store-parking)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_multi-store-parking&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=ivan-sanabria_multi-store-parking)

## Specifications

Please review the PDF file attached on the project.

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **CommandController.java**.

## Running Application on Terminal

To run the application on terminal with Maven:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean compile assembly:single
    java -jar target/multi-store-parking-2.0.0.jar examples/file_inputs.txt
```

To run the application on terminal with provided bash script:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    cd scripts && sh parking_lot.sh examples/file_inputs.txt
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Example of Supported Commands

These are the supported commands of the application:

```bash
    # Create a new parking lot with 6 slots
    create_parking_lot 6
    # Park a blue car with registration KA-01-HH-2701
    park KA-01-HH-2701 Blue
    # Car on slot 4 leaves the parking lot
    leave 4
    # Status of the parking slot
    status
    # Search car registration numbers with given colour
    registration_numbers_for_cars_with_colour White
    # Search slot numbers that contains cars with given colour
    slot_numbers_for_cars_with_colour White
    # Search slot numbers that contain a car with the given registration number
    slot_number_for_registration_number KA-01-HH-3141
```

# Contact Information

Email: icsanabriar@googlemail.com