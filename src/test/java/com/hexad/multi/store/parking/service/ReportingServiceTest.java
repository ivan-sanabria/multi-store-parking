/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.service;

import com.hexad.multi.store.parking.entity.Car;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

/**
 * Class to handle test cases for following operations:
 *
 * <ul>
 *  <li> get status or parking instance
 *  <li> search cars by color
 *  <li> search slots by color
 *  <li> get slot by registration
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ReportingServiceTest {

    /**
     * Define line separator for test cases.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Define instance for testing purposes.
     */
    private final ReportingService reportingService = new ReportingService();


    @Test
    public void get_empty_status() {

        final String format = "%1$-10s%2$-20s%3$s";
        final String expected = String.format(format, "Slot No.", "Registration No.", "Colour")
                .concat(LINE_SEPARATOR);

        final String status = reportingService.getStatus(new LinkedHashMap<>());

        assertEquals(expected, status);
    }

    @Test
    public void get_status_2_slots_2_parks() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, new Car("AA", "Black"));
        parking.put(2, new Car("AB", "Red"));

        final String status = reportingService.getStatus(parking);
        final String[] lines = status.split(LINE_SEPARATOR);

        assertEquals(3, lines.length);

        String result = lines[1].replaceAll("\\s+", "");
        String expected = "1AABlack";

        assertEquals(expected, result);

        result = lines[2].replaceAll("\\s+", "");
        expected = "2ABRed";

        assertEquals(expected, result);
    }

    @Test
    public void get_status_4_slots_3_parks_2_leaves() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, null);
        parking.put(2, null);
        parking.put(3, new Car("AB", "Red"));
        parking.put(4, null);

        final String status = reportingService.getStatus(parking);

        final String[] lines = status.split(LINE_SEPARATOR);
        assertEquals(2, lines.length);

        final String result = lines[1].replaceAll("\\s+", "");
        final String expected = "3ABRed";

        assertEquals(expected, result);
    }

    @Test
    public void get_registrations_for_white_cars_in_3_slots_2_parks_expected_0_hits() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, new Car("AB", "Blue"));
        parking.put(2, null);
        parking.put(3, new Car("AD", "Red"));

        final String results = reportingService.getRegistrationByColor(parking, "White");
        assertEquals(0, results.length());
    }

    @Test
    public void get_registrations_for_white_cars_in_3_slots_2_parks_expected_2_hits() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, new Car("AB", "White"));
        parking.put(2, null);
        parking.put(3, new Car("AD", "white"));

        final String results = reportingService.getRegistrationByColor(parking, "White");
        assertEquals("AB,AD", results);
    }

    @Test
    public void get_slots_for_white_cars_in_3_slots_2_parks_expected_0_hits() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, new Car("AB", "Blue"));
        parking.put(2, new Car("AD", "Red"));
        parking.put(3, null);

        final String results = reportingService.getSlotNumberByColor(parking, "White");
        assertEquals(0, results.length());
    }

    @Test
    public void get_slots_for_white_cars_in_3_slots_2_parks_expected_2_hits() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, new Car("AB", "White"));
        parking.put(2, new Car("AC", "White"));
        parking.put(3, null);

        final String results = reportingService.getSlotNumberByColor(parking, "White");
        assertEquals("1,2", results);
    }

    @Test
    public void get_slot_for_aaa_registration_in_3_slots_2_parks_expected_0_hits() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, new Car("AB", "White"));
        parking.put(2, new Car("AC", "Red"));
        parking.put(3, null);

        final String results = reportingService.getSlotNumberByRegistration(parking, "AAA");
        assertEquals("Not found", results);
    }

    @Test
    public void get_slot_for_aaa_registration_in_3_slots_2_parks_expected_1_hit() {

        final Map<Integer, Car> parking = new LinkedHashMap<>();

        parking.put(1, new Car("AB", "White"));
        parking.put(2, null);
        parking.put(3, new Car("AAA", "Red"));

        final String results = reportingService.getSlotNumberByRegistration(parking, "AAA");
        assertEquals("3", results);
    }

}
