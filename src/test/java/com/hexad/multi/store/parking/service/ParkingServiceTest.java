/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.service;

import com.hexad.multi.store.parking.entity.Car;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * Class to handle test cases for the following operations:
 *
 * <ul>
 *  <li> initialize parking
 *  <li> park car
 *  <li> leave car
 *  <li> retrieve current status of parking
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ParkingServiceTest {

    /**
     * Define instance for testing purposes.
     */
    private final ParkingService parkingService = new ParkingService();


    @Test
    public void initialize_5_slots_with_sort_keys_and_empty_car_instances() {

        final int n = 5;

        parkingService.initialize(n);

        final Map<Integer, Car> parking = parkingService.getParking();
        assertEquals(n, parking.size());

        final Iterator<Integer> iterator = parking.keySet().iterator();

        for (int a = 0; a < n; a++) {

            final Integer currentSlot = a + 1;
            assertEquals(currentSlot, iterator.next());
            assertNull(parking.get(a));
        }
    }

    @Test
    public void initialize_minus_5_slots_with_empty_parking() {

        final int n = -5;

        parkingService.initialize(n);

        final Map<Integer, Car> parking = parkingService.getParking();
        assertTrue(parking.isEmpty());
    }

    @Test
    public void initialize_2_slots_parking_1_null_car() {

        final int n = 2;

        parkingService.initialize(n);

        final int slot = parkingService.parkCar(null);
        assertEquals(0, slot);
    }

    @Test
    public void initialize_2_slots_parking_0_cars_leave_1_slot() {

        final int n = 2;

        parkingService.initialize(n);

        final boolean result = parkingService.leaveCar(1);
        assertFalse(result);
    }

    @Test
    public void initialize_5_slots_parking_3_cars() {

        final int n = 5;

        parkingService.initialize(n);

        final Car car1 = new Car("AAA", "Blue");
        int slot = parkingService.parkCar(car1);

        assertEquals(1, slot);

        final Car car2 = new Car("ABA", "Yellow");
        slot = parkingService.parkCar(car2);

        assertEquals(2, slot);

        final Car car3 = new Car("ACA", "Red");
        slot = parkingService.parkCar(car3);

        assertEquals(3, slot);
    }

    @Test
    public void initialize_3_slots_parking_5_cars() {

        final int n = 3;

        parkingService.initialize(n);

        final Car car1 = new Car("AAA", "Blue");
        int slot = parkingService.parkCar(car1);

        assertEquals(1, slot);

        final Car car2 = new Car("ABA", "Yellow");
        slot = parkingService.parkCar(car2);

        assertEquals(2, slot);

        final Car car3 = new Car("ACA", "Red");
        slot = parkingService.parkCar(car3);

        assertEquals(3, slot);

        final Car car4 = new Car("ADA", "Black");
        slot = parkingService.parkCar(car4);

        assertEquals(0, slot);

        final Car car5 = new Car("AEA", "Green");
        slot = parkingService.parkCar(car5);

        assertEquals(0, slot);
    }

    @Test
    public void initialize_4_slots_parking_3_cars_leave_2_cars() {

        final int n = 4;

        parkingService.initialize(n);

        final Car car1 = new Car("AAA", "Blue");
        int slot = parkingService.parkCar(car1);

        assertEquals(1, slot);

        final Car car2 = new Car("ABA", "Yellow");
        slot = parkingService.parkCar(car2);

        assertEquals(2, slot);

        final Car car3 = new Car("ACA", "Red");
        slot = parkingService.parkCar(car3);

        assertEquals(3, slot);

        int slotLeave = 1;
        boolean result = parkingService.leaveCar(slotLeave);

        assertTrue(result);

        Map<Integer, Car> parking = parkingService.getParking();
        assertNull(parking.get(slotLeave));

        slotLeave = 3;
        result = parkingService.leaveCar(slotLeave);

        assertTrue(result);

        parking = parkingService.getParking();
        assertNull(parking.get(slotLeave));
    }

    @Test
    public void initialize_2_slots_parking_2_cars_leave_3_invalid_slot() {

        final int n = 2;

        parkingService.initialize(n);

        final Car car1 = new Car("AAA", "Blue");
        int slot = parkingService.parkCar(car1);

        assertEquals(1, slot);

        final Car car2 = new Car("ABA", "Yellow");
        slot = parkingService.parkCar(car2);

        assertEquals(2, slot);

        int slotLeave = -1;
        boolean result = parkingService.leaveCar(slotLeave);

        assertFalse(result);

        slotLeave = 0;
        result = parkingService.leaveCar(slotLeave);

        assertFalse(result);

        slotLeave = 3;
        result = parkingService.leaveCar(slotLeave);

        assertFalse(result);
    }

    @Test
    public void initialize_2_slots_parking_1_cars_leave_1_empty_slot() {

        final int n = 2;

        parkingService.initialize(n);

        final Car car1 = new Car("AAA", "Blue");
        final int slot = parkingService.parkCar(car1);

        assertEquals(1, slot);

        final int slotLeave = 2;
        final boolean result = parkingService.leaveCar(slotLeave);

        assertFalse(result);
    }

}
