/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.service;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Class to handle test cases for following operations:
 *
 * <ul>
 *  <li> execute single command
 *  <li> execute multiple commands
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CommandServiceTest {

    /**
     * Define instance for testing purposes.
     */
    private final CommandService commandService = new CommandService();


    @Test
    public void execute_null_command() {

        final String expected = "Command not supported";
        final String result = commandService.execute(null);

        assertEquals(expected, result);
    }

    @Test
    public void execute_empty_command() {

        final String expected = "Command not supported";
        final String result = commandService.execute("");

        assertEquals(expected, result);
    }

    @Test
    public void execute_invalid_commands() {

        final String expected = "Command not supported";

        String result = commandService.execute("Hello World Command");
        assertEquals(expected, result);

        result = commandService.execute("create_parking_lot AAAA");
        assertEquals("Command not supported - For input string: \"AAAA\"", result);

        result = commandService.execute("park AAAA Black WRONG");
        assertEquals(expected, result);

        result = commandService.execute("leave 1 AAAA");
        assertEquals(expected, result);

        result = commandService.execute("status 1");
        assertEquals(expected, result);

        result = commandService.execute("registration_numbers_for_cars_with_colour White Black");
        assertEquals(expected, result);

        result = commandService.execute("slot_numbers_for_cars_with_colour White Black");
        assertEquals(expected, result);

        result = commandService.execute("slot_number_for_registration_number AAA BBB");
        assertEquals(expected, result);
    }

    @Test
    public void execute_multiple_commands_with_4_invalid_commands() {

        String expected = "Created a parking lot with 3 slots";
        String result = commandService.execute("create_parking_lot 3");

        assertEquals(expected, result);

        expected = "Allocated slot number: 1";
        result = commandService.execute("park AAA White");

        assertEquals(expected, result);

        expected = "Command not supported";
        result = commandService.execute("park ABA Black Yellow");

        assertEquals(expected, result);

        expected = "Error slot was free beforehand";
        result = commandService.execute("leave 2");

        assertEquals(expected, result);

        expected = "Command not supported - For input string: \"A\"";
        result = commandService.execute("leave A");

        assertEquals(expected, result);

        expected = "Command not supported";
        result = commandService.execute("registration_numbers_for_cars_with_colour Black Yellow");

        assertEquals(expected, result);

        expected = "1";
        result = commandService.execute("slot_numbers_for_cars_with_colour white");

        assertEquals(expected, result);

        expected = "1";
        result = commandService.execute("slot_number_for_registration_number AAA");

        assertEquals(expected, result);

        expected = "Slot number 1 is free";
        result = commandService.execute("leave 1");

        assertEquals(expected, result);
    }

    @Test
    public void execute_multiple_commands_with_0_invalid_commands() {

        String expected = "Created a parking lot with 3 slots";
        String result = commandService.execute("create_parking_lot 3");

        assertEquals(expected, result);

        expected = "Allocated slot number: 1";
        result = commandService.execute("park AAA White");

        assertEquals(expected, result);

        expected = "Allocated slot number: 2";
        result = commandService.execute("park ABA Black");

        assertEquals(expected, result);

        expected = "Allocated slot number: 3";
        result = commandService.execute("park ACA Blue");

        assertEquals(expected, result);

        expected = "Sorry, parking lot is full";
        result = commandService.execute("park ADA Red");

        assertEquals(expected, result);

        expected = "Slot number 2 is free";
        result = commandService.execute("leave 2");

        assertEquals(expected, result);

        expected = "AAA";
        result = commandService.execute("registration_numbers_for_cars_with_colour white");

        assertEquals(expected, result);

        expected = "1";
        result = commandService.execute("slot_numbers_for_cars_with_colour white");

        assertEquals(expected, result);

        expected = "1";
        result = commandService.execute("slot_number_for_registration_number AAA");

        assertEquals(expected, result);

        expected = "SlotNo.RegistrationNo.Colour1AAAWhite3ACABlue";
        result = commandService.execute("status");
        result = result.replaceAll("\\s+", "");

        assertEquals(expected, result);
    }

}
