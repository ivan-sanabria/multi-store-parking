/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.controller;

import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.testng.Assert.assertEquals;

/**
 * Class to handle test cases for following operations:
 *
 * <ul>
 *  <li> file argument
 *  <li> command lines
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CommandControllerTest {

    /**
     * Define line separator for test cases.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();


    @Test
    public void generate_io_exception_reading_file_input() {

        final String[] args = {"This file doesn't exists"};

        final String input = "create_parking_lot 1" + LINE_SEPARATOR +
                "park KA-01-HH-1234 White" + LINE_SEPARATOR +
                "park KA-01-HH-1235 Black" + LINE_SEPARATOR;

        final String expected = "";

        final ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setIn(in);
        System.setOut(new PrintStream(controllerOut));

        CommandController.main(args);
        assertEquals(expected, controllerOut.toString());
    }

    @Test
    public void generate_parking_slot_3_cars_1_park_1_leave_to_console() {

        final String[] args = {};

        final String input = "create_parking_lot 3" + LINE_SEPARATOR +
                "park KA-01-HH-1234 White" + LINE_SEPARATOR +
                "leave 1" + LINE_SEPARATOR;

        final String expected = "Created a parking lot with 3 slots" + LINE_SEPARATOR +
                "Allocated slot number: 1" + LINE_SEPARATOR +
                "Slot number 1 is free" + LINE_SEPARATOR;

        final ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setIn(in);
        System.setOut(new PrintStream(controllerOut));

        CommandController.main(args);
        assertEquals(expected, controllerOut.toString());
    }

    @Test
    public void generate_parking_slot_3_cars_1_leave_to_file() {

        final String[] args = {"./examples/file_tests.txt"};

        final String expected = "Created a parking lot with 3 slots" + LINE_SEPARATOR +
                "Allocated slot number: 1" + LINE_SEPARATOR +
                "Slot number 1 is free" + LINE_SEPARATOR;

        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(controllerOut));

        CommandController.main(args);
        assertEquals(expected, controllerOut.toString());
    }

    @Test
    @SuppressWarnings("AccessStaticViaInstance")
    public void generate_parking_slot_3_cars_1_park_1_leave_to_console_by_instance() {

        final String[] args = {};

        final String input = "create_parking_lot 1" + LINE_SEPARATOR +
                "park KA-01-HH-1234 White" + LINE_SEPARATOR +
                "park KA-01-HH-1235 Black" + LINE_SEPARATOR;

        final String expected = "Created a parking lot with 1 slots" + LINE_SEPARATOR +
                "Allocated slot number: 1" + LINE_SEPARATOR +
                "Sorry, parking lot is full" + LINE_SEPARATOR;

        final ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setIn(in);
        System.setOut(new PrintStream(controllerOut));

        final CommandController commandController = new CommandController();
        commandController.main(args);

        assertEquals(expected, controllerOut.toString());
    }

}
