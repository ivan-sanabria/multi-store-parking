/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.service;

import com.hexad.multi.store.parking.entity.Car;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

/**
 * Class to handle report requirements for following operations:
 *
 * <ul>
 *  <li> get status or parking instance
 *  <li> search cars by color
 *  <li> search slots by color
 *  <li> get slot by registration
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class ReportingService {

    /**
     * Retrieves the status of the parking.
     *
     * @param parking Map instance representing the parking.
     * @return A String containing the status of the parking.
     */
    String getStatus(final Map<Integer, Car> parking) {

        final String separator = System.lineSeparator();
        final String format = "%1$-10s%2$-20s%3$s";
        final String title = String.format(format, "Slot No.", "Registration No.", "Colour");

        final StringBuilder builder = new StringBuilder();

        builder.append(title);
        builder.append(separator);

        final Set<Integer> occupiedSlots = parking.keySet()
                .stream()
                .filter(position -> parking.get(position) != null)
                .collect(Collectors.toSet());

        occupiedSlots.forEach(key -> {

            final Car car = parking.get(key);

            builder.append(
                    String.format(format,
                            key,
                            car.getRegistration(),
                            car.getColor()));

            builder.append(separator);
        });

        return builder.toString();
    }

    /**
     * Retrieves the registrations of the cars in the parking that have the given color.
     *
     * @param parking Map instance representing the parking.
     * @param color   Color to search cars inside the parking instance.
     * @return A String containing the registrations of the cars with the given color.
     */
    String getRegistrationByColor(final Map<Integer, Car> parking, final String color) {

        return parking.values()
                .stream()
                .filter(car -> (null != car) && (car.getColor().equalsIgnoreCase(color)))
                .map(Car::getRegistration)
                .collect(joining(","));
    }

    /**
     * Retrieves the slot numbers of the parking that have a parked car with given color.
     *
     * @param parking Map instance representing the parking.
     * @param color   Color to search slots inside the parking instance.
     * @return A String containing the slots of the parking with the given color.
     */
    String getSlotNumberByColor(final Map<Integer, Car> parking, final String color) {

        return parking.entrySet()
                .stream()
                .filter(entry -> {
                    final Car car = entry.getValue();
                    return (null != car) && (car.getColor().equalsIgnoreCase(color));
                })
                .map(entry -> String.valueOf(entry.getKey()))
                .collect(joining(","));
    }

    /**
     * Retrieves the slot number of the parking that have a parked car with the given registration.
     *
     * @param parking      Map instance representing the parking.
     * @param registration Registration to search inside the parking instance.
     * @return A String containing the slot number of the parking with the given registration.
     */
    String getSlotNumberByRegistration(final Map<Integer, Car> parking, final String registration) {

        return parking.entrySet()
                .stream()
                .filter(entry -> {
                    final Car car = entry.getValue();
                    return (null != car) && (car.getRegistration().equalsIgnoreCase(registration));
                })
                .map(entry -> String.valueOf(entry.getKey()))
                .findFirst()
                .orElse("Not found");
    }

}
