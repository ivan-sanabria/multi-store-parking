/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.service;

import com.hexad.multi.store.parking.entity.Car;
import com.hexad.multi.store.parking.utils.ParkingCommand;

/**
 * Class to handle command operations and execute them using:
 *
 * <ul>
 *  <li> Parking Service
 *  <li> Reporting Service
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CommandService {

    /**
     * Define default constructor of CommandService.
     */
    public CommandService() {}

    /**
     * Define separator of arguments for commands.
     */
    private static final String SEPARATOR = " ";

    /**
     * Define base error for the command service.
     */
    private static final String BASE_ERROR = "Command not supported";

    /**
     * Define parking service instance to handle initialize, park, leave operations.
     */
    private final ParkingService parkingService = new ParkingService();

    /**
     * Define reporting service instance to handle get status, registration and slots by colors and slot by registration.
     */
    private final ReportingService reportingService = new ReportingService();

    /**
     * Receives the input of the console or file and execute the respective command.
     *
     * @param input Input in command line or file.
     * @return Result of the command operation to print it in console.
     */
    public String execute(final String input) {

        final String[] execution = (null == input) ? new String[1] : input.split(SEPARATOR);
        final String command = execution[0];
        final ParkingCommand parkingCommand = ParkingCommand.findByCommandAndArgsLength(command, execution.length - 1);

        // Switch is not used because NullPointerException and default case is difficult to cover on unit tests
        if (ParkingCommand.CREATE_PARKING_LOT.equals(parkingCommand))
            return createParkingLot(execution[1]);

        if (ParkingCommand.PARK_CAR.equals(parkingCommand))
            return parkCar(execution);

        if (ParkingCommand.LEAVE_CAR.equals(parkingCommand))
            return leaveCar(execution[1]);

        if (ParkingCommand.STATUS.equals(parkingCommand))
            return reportingService.getStatus(parkingService.getParking());

        if (ParkingCommand.SEARCH_REGISTRATION_COLOUR.equals(parkingCommand))
            return reportingService.getRegistrationByColor(parkingService.getParking(), execution[1]);

        if (ParkingCommand.SEARCH_SLOT_COLOUR.equals(parkingCommand))
            return reportingService.getSlotNumberByColor(parkingService.getParking(), execution[1]);

        if (ParkingCommand.SEARCH_SLOT_REGISTRATION.equals(parkingCommand))
            return reportingService.getSlotNumberByRegistration(parkingService.getParking(), execution[1]);

        return BASE_ERROR;
    }

    /**
     * Extraction of leave car command.
     *
     * @param arg Argument given as a program input.
     * @return Message is going to be displayed on the console after leave command is executed.
     */
    private String leaveCar(final String arg) {

        try {

            final int numberSlot = Integer.parseInt(arg);
            final boolean result = parkingService.leaveCar(numberSlot);

            return result ? String.format("Slot number %d is free", numberSlot) : "Error slot was free beforehand";

        } catch (NumberFormatException ne) {

            return BASE_ERROR + " - " + ne.getLocalizedMessage();
        }
    }

    /**
     * Extraction of park car command.
     *
     * @param args Arguments given as a program input.
     * @return Message is going to be displayed on the console after park command is executed.
     */
    private String parkCar(final String[] args) {

        final Car car = new Car(args[1], args[2]);
        final int slot = parkingService.parkCar(car);

        return (0 == slot) ? "Sorry, parking lot is full" : String.format("Allocated slot number: %d", slot);
    }

    /**
     * Extraction of creating parking lot.
     *
     * @param arg Argument given as a program input.
     * @return Message is going to be displayed on the console after creating parking lot command is executed.
     */
    private String createParkingLot(final String arg) {

        try {

            final int numberSlots = Integer.parseInt(arg);
            parkingService.initialize(numberSlots);

            return String.format("Created a parking lot with %s slots", numberSlots);

        } catch (NumberFormatException ne) {

            return BASE_ERROR + " - " + ne.getLocalizedMessage();
        }
    }

}
