/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.entity;

import lombok.Getter;

import java.io.Serializable;

/**
 * Class to store data of cars for following use cases:
 *
 * <ul>
 *  <li> generate automate tickets
 *  <li> report to government
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Getter
public class Car implements Serializable {

    /**
     * Number of registration. Example: KA-01-HH-1234
     */
    private String registration;

    /**
     * Color of the car. Example: White
     */
    private String color;

    /**
     * Constructor for Command Service used on park commands.
     *
     * @param registration Number of registration.
     * @param color        Color of the car.
     */
    public Car(String registration, String color) {
        this.registration = registration;
        this.color = color;
    }

}
