/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.service;

import com.hexad.multi.store.parking.entity.Car;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Class to handle parking requirements for following operations:
 *
 * <ul>
 *  <li> initialize parking
 *  <li> park car
 *  <li> leave car
 *  <li> retrieve current status of parking
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ParkingService {

    /**
     * Define default constructor of ParkingService.
     */
    ParkingService() {}

    /**
     * Map of represent the parking concept, were the key is the position of the slot and the value is the car occupy it.
     */
    private Map<Integer, Car> parking;

    /**
     * Initialize the parking instance of the service.
     *
     * @param n Number of slots that parking is able to hold up.
     */
    void initialize(final int n) {

        this.parking = new LinkedHashMap<>();

        for (int position = 0; position < n; position++) {
            parking.put(position + 1, null);
        }
    }

    /**
     * Retrieves the parking instance for reporting purposes. In case the parking is not initialize the instance is null.
     *
     * @return Map instance of the current status of the parking slots.
     */
    public Map<Integer, Car> getParking() {
        return parking;
    }

    /**
     * Park car method that assigns the given Car instance into an available slot and return the position of the slot.
     * In case the parking instance is full, this method return 0.
     *
     * @param car Car instance to assign to an empty slot.
     * @return Position of the slot in the parking instance
     */
    int parkCar(final Car car) {

        final Optional<Integer> available = parking.keySet()
                .stream()
                .filter(position -> (null == parking.get(position)))
                .findFirst();

        if (null != car && available.isPresent()) {

            final Integer position = available.get();
            parking.put(position, car);

            return position;

        } else {

            return 0;
        }
    }

    /**
     * Leave Car method that empty a slot for the given numberSlot. The method validates that numberSlot exists on the
     * parking instance and that is filled by a car instance to free it and return true, otherwise the method return false
     * and don't change any data on the parking instance.
     *
     * @param numberSlot Position of the slot to free it.
     * @return A boolean representing the result of the operation.
     */
    boolean leaveCar(final int numberSlot) {

        if (0 >= numberSlot || numberSlot > parking.size())
            return false;

        final Car car = parking.get(numberSlot);

        if (null == car)
            return false;

        parking.put(numberSlot, null);

        return true;
    }

}
