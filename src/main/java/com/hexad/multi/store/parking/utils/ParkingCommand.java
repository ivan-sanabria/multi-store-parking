/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.utils;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration to define supported commands for parking slots.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public enum ParkingCommand {

    /**
     * Definition to create a parking lot command.
     */
    CREATE_PARKING_LOT("create_parking_lot", 1),

    /**
     * Definition to park a car command.
     */
    PARK_CAR("park", 2),

    /**
     * Definition to leave a car command.
     */
    LEAVE_CAR("leave", 1),

    /**
     * Definition to parking lot status command.
     */
    STATUS("status", 0),

    /**
     * Definition to search registration numbers of cars with colour command.
     */
    SEARCH_REGISTRATION_COLOUR("registration_numbers_for_cars_with_colour", 1),

    /**
     * Definition to search slot numbers of cars with colour command.
     */
    SEARCH_SLOT_COLOUR("slot_numbers_for_cars_with_colour", 1),

    /**
     * Definition to slot number of a car with registration number command.
     */
    SEARCH_SLOT_REGISTRATION("slot_number_for_registration_number", 1);

    /**
     * Command line supported by the application.
     */
    @Getter
    private String command;

    /**
     * Number of arguments supported by the command.
     */
    @Getter
    private int argsLength;

    /**
     * Private constructor to define the parking commands.
     *
     * @param command    Command line.
     * @param argsLength Number of arguments that the command support.
     */
    ParkingCommand(final String command, final int argsLength) {
        this.command = command;
        this.argsLength = argsLength;
    }

    /**
     * Define command index for searching operations.
     */
    private static final Map<Integer, ParkingCommand> COMMAND_INDEX = new HashMap<>(ParkingCommand.values().length);

    /*
     * Fill the command index for searching operations.
     */
    static {

        for (ParkingCommand parkingCommand : ParkingCommand.values()) {

            final int argsLength = parkingCommand.getArgsLength();
            final String hashValue = parkingCommand.getCommand() + argsLength;

            COMMAND_INDEX.put(hashValue.hashCode(), parkingCommand);
        }
    }

    /**
     * Retrieves the Parking Command instance by the given command and args length.
     *
     * @param command    Command to find inside the index.
     * @param argsLength Arguments length supported by the command to find inside the index.
     * @return Parking Command found in the index, otherwise return null.
     */
    public static ParkingCommand findByCommandAndArgsLength(final String command, final int argsLength) {

        final String hashValue = command + argsLength;

        return COMMAND_INDEX.get(hashValue.hashCode());
    }

}
