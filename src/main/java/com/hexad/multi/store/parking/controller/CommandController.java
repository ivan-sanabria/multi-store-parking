/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hexad.multi.store.parking.controller;

import com.hexad.multi.store.parking.service.CommandService;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Class to handle command operations receiving input by:
 *
 * <ul>
 *  <li> file argument
 *  <li> command lines
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CommandController {

    /**
     * Define default constructor of CommandController.
     */
    public CommandController() {}

    /**
     * Main function to execute on jar file.
     *
     * @param args Arguments given to the program. Example: file_inputs.txt
     */
    public static void main(String... args) {

        if (1 == args.length) {

            final String filename = args[0];
            final File file = new File(filename);

            try (Scanner scanner = new Scanner(file)) {

                processInput(scanner);

            } catch (FileNotFoundException e) {

                final String errorMessage = String.format("Error reading file [%s] - [%s]",
                        filename,
                        e.getLocalizedMessage());

                System.err.println(errorMessage);
            }

        } else {

            processInput(new Scanner(System.in));
        }
    }

    /**
     * Process the input of the program using the given scanner instance.
     *
     * @param scanner Scanner instance used to read the input given to the application.
     */
    private static void processInput(Scanner scanner) {

        final CommandService commandService = new CommandService();

        while (scanner.hasNext()) {

            final String command = scanner.nextLine();
            final String result = commandService.execute(command);

            System.out.println(result);
        }
    }

}
